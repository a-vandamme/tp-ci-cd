function toInt(num) {
  return parseInt(num, 10);
}

const calcs = {
  x: (n1, n2) => toInt(n1) * toInt(n2),
  '/': (n1, n2) => toInt(n1) / toInt(n2),
  '+': (n1, n2) => toInt(n1) + toInt(n2),
  '-': (n1, n2) => toInt(n1) - toInt(n2),
};

function findFirstOperator(data) {
  const divIndex = data.indexOf('/') > -1 ? data.indexOf('/') : Infinity;
  const multIndex = data.indexOf('x') > -1 ? data.indexOf('x') : Infinity;
  if (divIndex === multIndex){
    return 1;
  }
  return Math.min(divIndex, multIndex);
}

export default function (entries) {
  const currentEntries = [...entries];
  while (currentEntries.length > 2) {
    const operator = findFirstOperator(currentEntries);
    const result = calcs[currentEntries[operator]](
      currentEntries[operator - 1],
      currentEntries[operator + 1],
    );
    currentEntries.splice(operator - 1, 3, result || 0);
  }
  return currentEntries.join(' ');
}
